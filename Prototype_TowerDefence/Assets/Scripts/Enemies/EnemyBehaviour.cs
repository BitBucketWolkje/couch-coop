﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    [SerializeField] private int startingHealth = 3;
     private GameObject HealthBar;

    public int _damage = 1;

    private healthBarScript healthBarScript;

    public bool OnFire;

    [SerializeField] private float _health;
    public float Health
    {
        get => _health; set
        {
            _health = value;
            healthBarScript.UpdateBar(_health / startingHealth);
            if (_health <= 0) Die();
        }
    }
    [SerializeField] GameObject _coin;

    private float _timer;
    // Update is called once per frame

    private void Start()
    {
        healthBarScript = GetComponentInChildren<healthBarScript>();
        Health = startingHealth;
        _timer = 2f;
    }

    private void Update()
    {
        if (OnFire)
        {
            _timer += Time.deltaTime;
        }

        if (_timer > 2)
        {
            Health -= 0.25f;
            _timer = 0f;
        }
    }
    private void Die()
    {
        Instantiate(_coin, transform.position + Vector3.up, Quaternion.Euler(90,0,0));
        BuildingBehaviour.EnemiesInRange.Remove(this.transform);
        Destroy(this.gameObject);
    }

    public IEnumerator ExtinguishFire()
    {
        yield return new WaitForSeconds(4);
        OnFire = false;
        _timer = 2f;
    }
}
