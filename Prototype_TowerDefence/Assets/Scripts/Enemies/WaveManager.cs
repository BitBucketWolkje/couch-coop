﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class WaveManager : MonoBehaviour
{
    private int _wave = 0;
    [SerializeField] private int _waveSize = 1;
    [SerializeField] private GameObject[] _specialEnemies;
    [SerializeField] private GameObject _normalEnemy;
    [SerializeField] private int _ExtraEnemiesPerWave = 1;
    [SerializeField] private float _TimeBetweenEnemies = 2;
    [SerializeField] private int _breakTimer = 5;
    [SerializeField] private Slider _bar;
    [SerializeField] private Text _display;

    private float timer = 0;


    public static List<PathingBehaviour> ENEMYLIST = new List<PathingBehaviour>();

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Pause());
    }

    private IEnumerator Pause()
    {
        

        _display.text = "break";
        while (timer < _breakTimer)
        {
            timer += Time.deltaTime;
            _bar.value = 3*(float)timer / (float)_breakTimer;
            yield return null;
        }
        timer = 0;
        StartCoroutine(Wave());
    }

    private IEnumerator Wave()
    {
        _display.text = ("Wave " + _wave);
        for (int i = 0; i < _waveSize; i++)
        {
            GameObject currentEnemy;
            if (_wave % 3 == 0 && _wave != 0)
            {
                currentEnemy = Instantiate(_specialEnemies[Random.Range(0, _specialEnemies.Length)], transform);
                i++;
            }
            else
            {
                currentEnemy = Instantiate(_normalEnemy, transform);
            }
            ENEMYLIST.Add(currentEnemy.GetComponent<PathingBehaviour>());
            _bar.value = 3* (float)i +1 / (float)_waveSize;
            yield return new WaitForSeconds(_TimeBetweenEnemies);
        }
        _waveSize += _ExtraEnemiesPerWave;
        _wave++;
        yield return null;
        StartCoroutine(Pause());
    }
    

}
