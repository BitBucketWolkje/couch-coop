﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ConsoleManager : MonoBehaviour
{

    private string[] joysticks;

    [SerializeField]
    private WaitLobby[] _players = new WaitLobby[3];

    private List<int> activePlayers = new List<int>();

    [SerializeField] private GameObject _pauseMenu;
    [SerializeField] private CameraBehaviour _cameraBehaviour;
    [SerializeField] private WaveManager _waveSpawner;
    [SerializeField] private int _amountOfPlayers;

    // Update is called once per frame
    void Update()
    {
        for (int i = 1; i < _players.Length+1; ++i)
        {
            if (!activePlayers.Contains(i) && Input.GetButtonDown("Select_Player" + (i)))
            {
                activePlayers.Add(i);
                _players[GetPlayer()].Activate(i);
                joysticks = Input.GetJoystickNames();
            }
        }

        if (activePlayers.Count == _amountOfPlayers)
        {
           StartGame();
        }
    }

    private int GetPlayer()
    {
        for (int i = 0; i < _players.Length; ++i)
        {
            if (!_players[i].enabled)
            {
                return i;
            }
        }
        return 0;
    }

    private void StartGame()
    {
        foreach (var player in _players)
        {
            player.gameObject.tag = "Player";
            player.StartGame();
        }

        _pauseMenu.SetActive(true);
        _waveSpawner.enabled = true;
        _cameraBehaviour.enabled = true;
        this.enabled = false;
        this.transform.GetChild(0).gameObject.SetActive(false);
    }
}
