﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GAMEPAUSED = false;

    [SerializeField] private GameObject _pauseMenuUI;
    [SerializeField] private EventSystem _eventSystem;

    private int _playerInControl;

    // Update is called once per frame
    void Update()
    {
        if (!GAMEPAUSED)
        {
             if (Input.GetButtonDown("Select_Player1"))
             {
                 PauseGame(1);
                return;
             }
             else if (Input.GetButtonDown("Select_Player2"))
             {
                 PauseGame(2);
                 return;
             }
             else if (Input.GetButtonDown("Select_Player3")) 
             {
                 PauseGame(3);
                 return;
             }
        }

        if (GAMEPAUSED)
        {
            if (Input.GetButtonDown("Select_Player" + _playerInControl))
            {
                ResumeGame();
            }
        }
    }

    public void ResumeGame()
    {
        _playerInControl = 0;
        _pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GAMEPAUSED = false;
    }

    private void PauseGame(int player)
    {
        _eventSystem.GetComponent<StandaloneInputModule>().horizontalAxis = "LStickHorizontal" + player;
        _eventSystem.GetComponent<StandaloneInputModule>().verticalAxis = "LStickVertical" + player;
        _eventSystem.GetComponent<StandaloneInputModule>().submitButton = "Button_A" + player;
        _eventSystem.GetComponent<StandaloneInputModule>().cancelButton = "Button_B" + player;
        _playerInControl = player;

        _pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GAMEPAUSED = true;
    }

    public void MainMenu()
    {
        _playerInControl = 0;
        _pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GAMEPAUSED = false;
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
