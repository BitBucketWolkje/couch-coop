﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

    public class StartUp : MonoBehaviour
    {
        public WorkshopBehaviour Workshop;
        [SerializeField] private GameObject _wood;
        [SerializeField] private GameObject _stone;

        public GameObject Wood { get => _wood; }
        public GameObject Stone
        {
            get => _stone;
        }


    //private void Start()
    //    {
    //        Workshop = GameObject.FindGameObjectWithTag("Workshop").GetComponent<WorkshopBehaviour>();
    //    }
}
