﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostScripts : MonoBehaviour
{
    private Vector3 _direction;
     private Transform _target;
    [SerializeField] private float Speed;
    // Start is called before the first frame update
    void Start()
    {
     _target = GameObject.FindGameObjectWithTag("Target").transform;
        _direction = _target.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Time.deltaTime * Speed * _direction * Time.deltaTime);
    }
}
