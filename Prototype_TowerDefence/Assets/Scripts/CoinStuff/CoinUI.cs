﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CoinUI : MonoBehaviour
{
    private TextMeshProUGUI _text;
    public static int COINSCOLLECTED = 0;
    void Start()
    {
         _text= GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        _text.text = COINSCOLLECTED.ToString();
    }
}
