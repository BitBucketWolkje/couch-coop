﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CoinCollectScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        CoinCollected(other);
    }

    private void CoinCollected(Collider other)
    {
        if (other.tag == "Coin")
        {
            Destroy(other.gameObject);
            CoinUI.COINSCOLLECTED++;

        }
    }

}
