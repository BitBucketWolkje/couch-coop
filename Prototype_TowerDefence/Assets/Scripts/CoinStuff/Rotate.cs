﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

    public Vector3 rot = Vector3.zero;

    private void Start()
    {
        if(!this.CompareTag("Workshop"))
        StartCoroutine(Despawn());
    }
	void Update () {
        this.transform.Rotate(rot*Time.deltaTime);
	}

    private IEnumerator Despawn()
    {
        yield return new WaitForSeconds(20);
        Destroy(this.gameObject);
    }
}
