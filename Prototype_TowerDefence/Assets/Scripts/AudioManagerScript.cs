﻿using UnityEngine.Audio;
using UnityEngine;
using System;

public class AudioManagerScript : MonoBehaviour
{
    
    public Sound[] Sounds;
    // Start is called before the first frame update
    void Awake()
    {
        Cursor.visible = false;
        foreach (Sound s in Sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }


    }

    private void Singleton()
    {
        AudioManagerScript[] ScriptsinScene = FindObjectsOfType<AudioManagerScript>();
        for (int i = 0; i < ScriptsinScene.Length; i++)
        {
            if (ScriptsinScene[i] != this)
            {
                Destroy(gameObject);
            }
        }
    }


    private void Start()
    {
        Singleton();
        DontDestroyOnLoad(this.gameObject);
         Play("MainMusic");
    }
    public void Play(string name)
    {
        Sound s = Array.Find(Sounds, sound => sound.name == name);
        if (s == null) return;
        s.source.Play();
    }

    public void StopPlaying(string sound)
    {
        Sound s = Array.Find(Sounds, item => item.name == sound);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
            return;
        }


        s.source.Stop();
    }
}
