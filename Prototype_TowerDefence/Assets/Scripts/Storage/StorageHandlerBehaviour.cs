﻿using UnityEngine;

public class StorageHandlerBehaviour : MonoBehaviour
{
    [SerializeField]
    private int _startingStone;

    [SerializeField]
    private int _startingWood;

    private void Awake()
    {
        StoneStorageBehaviour.StoneCount = _startingStone;
        WoodStorageBehaviour.WoodCount = _startingWood;
    }
}