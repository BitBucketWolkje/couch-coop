﻿using TMPro;
using UnityEngine;

public class WoodStorageBehaviour : MonoBehaviour
{
    public static int WoodCount; //The wood is separated between all the storages

    public bool IsBuilt { get => _isBuilt; }

    [SerializeField]
    private bool _isBuilt;

    [SerializeField]
    private TextMeshProUGUI _woodStorageText;

    private int _requiredWood = 1;
    private int _storedWood;

    void Update()
    {
        CheckWhenBuilt();
        StorageFuntionality();
    }

    private void CheckWhenBuilt()
    {
        if (_storedWood == _requiredWood)
            _isBuilt = true;
    }

    private void StorageFuntionality()
    {
        if (_isBuilt)
        {
            _woodStorageText.text = "Wood: " + WoodCount.ToString();

            _woodStorageText.transform.LookAt(Camera.main.transform);
        }
        else
            _woodStorageText.text = "W: " + _storedWood.ToString() + "/" + _requiredWood.ToString();
    }

    public void ConstructStorage(int material)
    {
        if (!_isBuilt)
            _storedWood += material;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Tile"))
        {
            other.GetComponent<TileBehaviour>().TileState = TileState.Full;
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        this.GetComponent<Rigidbody>().isKinematic = true;
    }
}