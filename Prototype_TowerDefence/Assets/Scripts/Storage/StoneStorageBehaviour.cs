﻿using System;
using TMPro;
using UnityEngine;

public class StoneStorageBehaviour : MonoBehaviour
{
    public static int StoneCount;

    public bool IsBuilt { get => _isBuilt; }

    [SerializeField]
    private bool _isBuilt;

    [SerializeField]
    private TextMeshProUGUI _stoneStorageText;

    private int _requiredStone = 1;
    private int _storedStone;

    void Update()
    {
        CheckWhenBuilt();
        StorageFuntionality();
    }

    private void CheckWhenBuilt()
    {
        if (_storedStone == _requiredStone)
            _isBuilt = true;
    }

    private void StorageFuntionality()
    {
        if (_isBuilt)
        {
            _stoneStorageText.text = "Stone: " + StoneCount.ToString();

            _stoneStorageText.transform.LookAt(Camera.main.transform);
        }
        else
            _stoneStorageText.text = "S: " + _storedStone.ToString() + "/" + _requiredStone.ToString();
    }

    public void ConstructStorage(int material)
    {
        if (!_isBuilt)
            _storedStone += material;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Tile"))
        {
            other.GetComponent<TileBehaviour>().TileState = TileState.Full;

        }
    }

    private void OnCollisionEnter(Collision other)
    {
        this.GetComponent<Rigidbody>().isKinematic = true;
    }
}