﻿using System.Collections;
using UnityEngine;

public class PathingBehaviour : MonoBehaviour
{
    public bool ShouldRequestPath = true;

    [SerializeField] private float _startSpeed;
    [SerializeField] private float _turnSpeed;
    [SerializeField] private float _turnDist;

    private PathBehaviour _path;
    private Transform _target;

    public bool Frozen;
    private float _speed;


    private void Start()
    {
        _target = GameObject.FindGameObjectWithTag("Target").transform;
        _speed = _startSpeed;
    }

    private void Update()
    {
        if (ShouldRequestPath)
        {
            PathRequestBehaviour.RequestPath(transform.position, _target.position, OnPathFound);
            ShouldRequestPath = false;
        }

        if (Frozen)
        {
            _speed = _startSpeed / 2;
        }
    }

    public void OnPathFound(Vector3[] waypoints, bool pathSuccessful)
    {
        if (pathSuccessful)
        {
            _path = new PathBehaviour(waypoints, transform.position, _turnDist);

            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath");
        }
    }

    private IEnumerator FollowPath()
    {
        bool isFollowingPath = true;
        int pathIndex = 0;

        transform.LookAt(_path.LookPoints[0]);

        while (isFollowingPath)
        {
            Vector2 position2D = new Vector2(transform.position.x, transform.position.z);

            while (_path.TurnBoundaries[pathIndex].HasCrossedLine(position2D))
            {
                if (pathIndex == _path.FinishLineIndex)
                {
                    isFollowingPath = false;
                    break;
                }
                else
                    pathIndex++;
            }

            if (isFollowingPath)
            {
                Quaternion targetRotation = Quaternion.LookRotation(_path.LookPoints[pathIndex] - transform.position);
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * _turnSpeed);
                transform.Translate(Vector3.forward * Time.deltaTime * _speed, Space.Self);
            }

            yield return null;
        }
    }

    public IEnumerator Defrost()
    {
        yield return new WaitForSeconds(2);
        Frozen = false;
        _speed = _startSpeed;
    }
}