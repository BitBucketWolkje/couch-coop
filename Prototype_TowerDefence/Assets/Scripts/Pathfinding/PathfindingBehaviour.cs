﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PathFindingBehaviour : MonoBehaviour
{
    private PathRequestBehaviour _pathRequest;
    private NodeGridBehaviour _nodeGrid;

    private bool _isPathSuccessful = false;

    public void StartFindPath(Vector3 startPosition, Vector3 targetPosition) => StartCoroutine(FindPath(startPosition, targetPosition));
    public bool IsPathSuccesfull() => _isPathSuccessful;

    private void Awake()
    {
        _pathRequest = GetComponent<PathRequestBehaviour>();
        _nodeGrid = GetComponent<NodeGridBehaviour>();
    }

    private IEnumerator FindPath(Vector3 startPosition, Vector3 targetPosition)
    {
        Vector3[] waypoints = new Vector3[0];

        _isPathSuccessful = false;

        NodeBehaviour startNode = _nodeGrid.NodeFromWorldPoint(startPosition);
        NodeBehaviour targetNode = _nodeGrid.NodeFromWorldPoint(targetPosition);

        if (startNode.Walkable && targetNode.Walkable)
        {
            HeapBehaviour<NodeBehaviour> openList = new HeapBehaviour<NodeBehaviour>(_nodeGrid.MaxSize);
            HashSet<NodeBehaviour> closedList = new HashSet<NodeBehaviour>();

            openList.Add(startNode);

            while (openList.Count > 0)
            {
                NodeBehaviour currentNode = openList.RemoveFirstItem();

                closedList.Add(currentNode);

                if (currentNode == targetNode)
                {
                    _isPathSuccessful = true;
                    break;
                }

                foreach (NodeBehaviour neighbourNode in _nodeGrid.GetNeighbours(currentNode))
                {
                    if (!neighbourNode.Walkable || closedList.Contains(neighbourNode))
                        continue;

                    int newMovementCostToNeighbour = currentNode.GCost + GetDistance(currentNode, neighbourNode) + neighbourNode.MovementPenalty;

                    if (newMovementCostToNeighbour < neighbourNode.GCost || !openList.IsContaining(neighbourNode))
                    {
                        neighbourNode.GCost = newMovementCostToNeighbour;
                        neighbourNode.HCost = GetDistance(neighbourNode, targetNode);
                        neighbourNode.Parent = currentNode;

                        if (!openList.IsContaining(neighbourNode))
                            openList.Add(neighbourNode);
                        else
                            openList.UpdateItem(neighbourNode);
                    }
                }
            }
        }
        
        yield return null;

        if (_isPathSuccessful)
            waypoints = RetracePath(startNode, targetNode);

        _pathRequest.FinishedProcessingPath(waypoints, _isPathSuccessful);
    }

    private Vector3[] RetracePath(NodeBehaviour startNode, NodeBehaviour endNode)
    {
        List<NodeBehaviour> path = new List<NodeBehaviour>();
        NodeBehaviour currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.Parent;
        }

        Vector3[] waypoints = SimplifiedPath(path);
        Array.Reverse(waypoints);

        return waypoints;
    }

    private Vector3[] SimplifiedPath(List<NodeBehaviour> path)
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 oldDirection = Vector2.zero;

        for (int i = 1; i < path.Count; i++)
        {
            Vector2 newDirection = new Vector2(path[i - 1].GridX - path[i].GridX, path[i - 1].GridY - path[i].GridY);

            if (newDirection != oldDirection)
                waypoints.Add(path[i - 1].WorldPosition);

            oldDirection = newDirection;
        }

        return waypoints.ToArray();
    }

    private int GetDistance(NodeBehaviour nodeA, NodeBehaviour nodeB)
    {
        int distX = Mathf.Abs(nodeA.GridX - nodeB.GridX);
        int distY = Mathf.Abs(nodeA.GridY - nodeB.GridY);

        if (distX > distY)
            return 14 * distY + 10 * (distX - distY);
        else
            return 14 * distX + 10 * (distY - distX);
    }

    public bool IsPathPosible(Vector3 startPosition, Vector3 targetPosition)
    {
        Vector3[] waypoints = new Vector3[0];

        NodeBehaviour startNode = _nodeGrid.NodeFromWorldPoint(startPosition);
        NodeBehaviour targetNode = _nodeGrid.NodeFromWorldPoint(targetPosition);

        if (startNode.Walkable && targetNode.Walkable)
        {
            HeapBehaviour<NodeBehaviour> openList = new HeapBehaviour<NodeBehaviour>(_nodeGrid.MaxSize);
            HashSet<NodeBehaviour> closedList = new HashSet<NodeBehaviour>();

            openList.Add(startNode);

            while (openList.Count > 0)
            {
                NodeBehaviour currentNode = openList.RemoveFirstItem();

                closedList.Add(currentNode);

                if (currentNode == targetNode)
                {
                    return true;
                }

                foreach (NodeBehaviour neighbourNode in _nodeGrid.GetNeighbours(currentNode))
                {
                    if (!neighbourNode.Walkable || closedList.Contains(neighbourNode))
                        continue;

                    int newMovementCostToNeighbour = currentNode.GCost + GetDistance(currentNode, neighbourNode) + neighbourNode.MovementPenalty;

                    if (newMovementCostToNeighbour < neighbourNode.GCost || !openList.IsContaining(neighbourNode))
                    {
                        neighbourNode.GCost = newMovementCostToNeighbour;
                        neighbourNode.HCost = GetDistance(neighbourNode, targetNode);
                        neighbourNode.Parent = currentNode;

                        if (!openList.IsContaining(neighbourNode))
                            openList.Add(neighbourNode);
                        else
                            openList.UpdateItem(neighbourNode);
                    }
                }
            }
        }

        return false;
    }
}