﻿using UnityEngine;

public struct LineBehaviour
{
    private Vector2 _pointOnLine1;
    private Vector2 _pointOnLine2;

    private const float _verticalLineGradient = 1e5f;

    private float _gradient;
    private float _yIntercept;
    private float _gradientPerpendicular;

    private bool _isApproachingSide;

    private bool ShouldGetSide(Vector2 p) => (p.x - _pointOnLine1.x) * (_pointOnLine2.y - _pointOnLine1.y) > (p.y - _pointOnLine1.y) * (_pointOnLine2.x - _pointOnLine1.x);

    public bool HasCrossedLine(Vector2 p) => ShouldGetSide(p) != _isApproachingSide;

    public LineBehaviour(Vector2 pointOnLine, Vector2 pointPerpendicularToLine)
    {
        float deltaX = pointOnLine.x - pointPerpendicularToLine.x;
        float deltaY = pointOnLine.y - pointPerpendicularToLine.y;

        if (deltaX == 0)
            _gradientPerpendicular = _verticalLineGradient;
        else
            _gradientPerpendicular = deltaY / deltaX;

        if (_gradientPerpendicular == 0)
            _gradient = _verticalLineGradient;
        else
            _gradient = -1 / _gradientPerpendicular;

        _yIntercept = pointOnLine.y - _gradient * pointOnLine.x;

        _pointOnLine1 = pointOnLine;
        _pointOnLine2 = pointOnLine + new Vector2(1f, _gradient);

        _isApproachingSide = false;
        _isApproachingSide = ShouldGetSide(pointPerpendicularToLine);
    }
}