﻿using UnityEngine;

public class NodeBehaviour : IHeapItem<NodeBehaviour>
{
    public bool Walkable;
    public Vector3 WorldPosition;

    public int GridX;
    public int GridY;
    public int GCost;
    public int HCost;
    public int MovementPenalty;

    private int _heapIndex;

    public NodeBehaviour Parent;

    public int FCost { get { return GCost + HCost; } }
    public int HeapIndex { get { return _heapIndex; } set { _heapIndex = value; } }

    public NodeBehaviour(bool walkable, Vector3 worldPosition, int gridX, int gridY, int movementPenalty)
    {
        Walkable = walkable;
        WorldPosition = worldPosition;
        GridX = gridX;
        GridY = gridY;
        MovementPenalty = movementPenalty;
    }

    public int CompareTo(NodeBehaviour nodeToCompare)
    {
        int compare = FCost.CompareTo(nodeToCompare.FCost);

        if (compare == 0)
            compare = HCost.CompareTo(nodeToCompare.HCost);

        return -compare;
    }
}