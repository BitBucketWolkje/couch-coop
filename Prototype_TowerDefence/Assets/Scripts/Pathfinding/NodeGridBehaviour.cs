﻿using System.Collections.Generic;
using UnityEngine;

public class NodeGridBehaviour : MonoBehaviour
{
    [System.Serializable]
    public class TerrainType
    {
        public LayerMask TerrainMask;
        public int TerrainPenalty;
    }

    [SerializeField] private GameObject _tilePrefab;

    public int MaxSize { get { return _gridSizeX * _gridSizeY; } }

    [SerializeField] private bool _shouldDisplayGridGizmos;
    [SerializeField] private LayerMask _impassableMask;
    [SerializeField] private Vector2 _gridWorldSize;
    [SerializeField] private float _nodeRadius;
    [SerializeField] private TerrainType[] _passableRegions;
    [SerializeField] private int _obstacleProximityPenalty;

    private Dictionary<int, int> _passableRegionsDictionary = new Dictionary<int, int>();

    private LayerMask _passableMask;

    private NodeBehaviour[,] _nodeGrid;

    private int _gridSizeX;
    private int _gridSizeY;
    
    private float _nodeDiameter;

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(_gridWorldSize.x, 1, _gridWorldSize.y));

        if (_nodeGrid != null && _shouldDisplayGridGizmos)
        {
            foreach (NodeBehaviour n in _nodeGrid)
            {
                Gizmos.color = (n.Walkable) ? Color.white : Color.red;
                Gizmos.DrawCube(n.WorldPosition, Vector3.one * (_nodeDiameter - .1f));
            }
        }
    }

    public List<NodeBehaviour> GetNeighbours(NodeBehaviour node)
    {
        List<NodeBehaviour> neighbours = new List<NodeBehaviour>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                    continue;

                int checkX = node.GridX + x;
                int checkY = node.GridY + y;

                if (checkX >= 0 && checkX < _gridSizeX && checkY >= 0 && checkY < _gridSizeY)
                    neighbours.Add(_nodeGrid[checkX, checkY]);


            }
        }

        return neighbours;
    }

    public NodeBehaviour NodeFromWorldPoint(Vector3 worldPosition)
    {
        float percentX = (worldPosition.x + _gridWorldSize.x / 2) / _gridWorldSize.x;
        float percentY = (worldPosition.z + _gridWorldSize.y / 2) / _gridWorldSize.y;

        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((_gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((_gridSizeY - 1) * percentY);

        return _nodeGrid[x, y];
    }

    private void Awake()
    {
        _nodeDiameter = _nodeRadius * 2f;

        _gridSizeX = Mathf.RoundToInt(_gridWorldSize.x / _nodeDiameter);
        _gridSizeY = Mathf.RoundToInt(_gridWorldSize.y / _nodeDiameter);

        foreach (TerrainType region in _passableRegions)
        {
            _passableMask.value |= region.TerrainMask.value;
            _passableRegionsDictionary.Add((int)Mathf.Log(region.TerrainMask.value, 2), region.TerrainPenalty);
        }

        CreateGrid();
    }

    private void CreateGrid()
    {
        _nodeGrid = new NodeBehaviour[_gridSizeX, _gridSizeY];

        Vector3 worldBottomLeft = transform.position - Vector3.right * _gridWorldSize.x / 2f - Vector3.forward * _gridWorldSize.y / 2f;

        for (int x = 0; x < _gridSizeX; x++)
        {
            for (int y = 0; y < _gridSizeY; y++)
            {

                //A* Grid
               
                Vector3 AStarWorldPoint = worldBottomLeft + Vector3.right * (x * _nodeDiameter + _nodeRadius) + Vector3.forward * (y * _nodeDiameter + _nodeRadius);
                bool passable = !(Physics.CheckSphere(AStarWorldPoint, _nodeRadius, _impassableMask));
                int movementPenalty = 0;

                Ray ray = new Ray(AStarWorldPoint + Vector3.up * 50, Vector3.down);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100, _passableMask))
                    _passableRegionsDictionary.TryGetValue(hit.collider.gameObject.layer, out movementPenalty);

                if (!passable)
                    movementPenalty += _obstacleProximityPenalty;

                _nodeGrid[x, y] = new NodeBehaviour(passable, AStarWorldPoint, x, y, movementPenalty);
            }
        }

        for (int x = 0; x < _gridSizeX/4; x++)
        {
            for (int y = 0; y < _gridSizeY/4; y++)
            {
              
                Vector3 prefabWorldPoint = worldBottomLeft + Vector3.right * (x * _nodeDiameter + _nodeRadius)*4 + Vector3.forward * (y * _nodeDiameter + _nodeRadius)*4;
                GameObject.Instantiate(_tilePrefab, prefabWorldPoint, Quaternion.identity, this.transform);       //Evt in list zetten als we tile nodig hebben

            }
        }

        BlurPenaltyMap(3);
    }

    private void BlurPenaltyMap(int blurSize)
    {
        int kernelSize = blurSize * 2 + 1;
        int kernelExtents = (kernelSize - 1) / 2;
        int[,] penaltiesHorizontalPass = new int[_gridSizeX, _gridSizeY];
        int[,] penaltiesVerticalPass = new int[_gridSizeX, _gridSizeY];

        for (int y = 0; y < _gridSizeY; y++)
        {
            for (int x = -kernelExtents; x <= kernelExtents; x++)
            {
                int sampleX = Mathf.Clamp(x, 0, kernelExtents);
                penaltiesHorizontalPass[0, y] += _nodeGrid[sampleX, y].MovementPenalty;
            }

            for (int x = 1; x < _gridSizeX; x++)
            {
                int removeIndex = Mathf.Clamp(x - kernelExtents - 1, 0, _gridSizeX);
                int addIndex = Mathf.Clamp(x + kernelExtents, 0, _gridSizeX - 1);
                penaltiesHorizontalPass[x, y] = penaltiesHorizontalPass[x - 1, y] - _nodeGrid[removeIndex, y].MovementPenalty + _nodeGrid[addIndex, y].MovementPenalty;
            }
        }

        for (int x = 0; x < _gridSizeX; x++)
        {
            for (int y = -kernelExtents; y <= kernelExtents; y++)
            {
                int sampleY = Mathf.Clamp(y, 0, kernelExtents);
                penaltiesVerticalPass[x, 0] += penaltiesHorizontalPass[x, sampleY];
            }

            for (int y = 1; y < _gridSizeY; y++)
            {
                int removeIndex = Mathf.Clamp(y - kernelExtents - 1, 0, _gridSizeY);
                int addIndex = Mathf.Clamp(y + kernelExtents, 0, _gridSizeY - 1);
                penaltiesVerticalPass[x, y] = penaltiesVerticalPass[x, y - 1] - penaltiesHorizontalPass[x, removeIndex] + penaltiesHorizontalPass[x, addIndex];

                int blurredPenalty = Mathf.RoundToInt((float)penaltiesVerticalPass[x, y] / (kernelSize * kernelSize));
                _nodeGrid[x, y].MovementPenalty = blurredPenalty;
            }
        }
    }

    private void Update()
    {
        foreach (NodeBehaviour node in _nodeGrid)
            node.Walkable = !(Physics.CheckSphere(node.WorldPosition, _nodeRadius, _impassableMask));
    }
}