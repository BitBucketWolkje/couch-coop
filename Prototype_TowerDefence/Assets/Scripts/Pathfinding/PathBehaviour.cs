﻿using UnityEngine;

public class PathBehaviour
{
    public readonly Vector3[] LookPoints;
    public readonly LineBehaviour[] TurnBoundaries;
    public readonly int FinishLineIndex;

    private Vector2 Vector3ToVector2(Vector3 vector3) => new Vector2(vector3.x, vector3.z);

    public PathBehaviour(Vector3[] waypoints, Vector3 startPosition, float turnDist)
    {
        LookPoints = waypoints;
        TurnBoundaries = new LineBehaviour[LookPoints.Length];
        FinishLineIndex = TurnBoundaries.Length - 1;

        Vector2 previousPoint = Vector3ToVector2(startPosition);

        for (int i = 0; i < LookPoints.Length; i++)
        {
            Vector2 currentPoint = Vector3ToVector2(LookPoints[i]);
            Vector2 directionToCurrentPoint = (currentPoint - previousPoint).normalized;
            Vector2 turnBoundaryPoint = (i == FinishLineIndex)?currentPoint : currentPoint - directionToCurrentPoint * turnDist;

            TurnBoundaries[i] = new LineBehaviour(turnBoundaryPoint, previousPoint - directionToCurrentPoint * turnDist);
            previousPoint = turnBoundaryPoint;
        }
    }
}