﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeBehaviour : MonoBehaviour
{

    [SerializeField] private BoxCollider[] _boxColliders;
    [SerializeField] private BoxCollider _fallCollider;
    private void OnTriggerEnter(Collider other)
    {
       
        if (other.gameObject.CompareTag("Tile"))
        {
            other.GetComponent<TileBehaviour>().TileState = TileState.Full;
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        this.GetComponent<Rigidbody>().isKinematic = true;
        foreach (var boxCollider in _boxColliders)
        {
            boxCollider.enabled = true;
        }

        _fallCollider.enabled = false;
    }
}
