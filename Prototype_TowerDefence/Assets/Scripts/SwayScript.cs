﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwayScript : MonoBehaviour
    {
    [SerializeField] private Vector3 _amplitude;
    [SerializeField] private Vector3 _rotAmplitude;
    [SerializeField] private Vector3 _frequency;
    [SerializeField] private Vector3 _rotFrequency;
    [SerializeField] private float _offset;
    [SerializeField] public float _lerp = 99;

    private Vector3 _startPosition;
    private Vector3 _startRot;
    private float _freqMulti;
    private float _amplMulti;
    private Vector3 _posMov;
    private Vector3 _rotMov;

    public float FreqMultiplier { get; set; }
    public float AmplitudeModifier { get; private set; }

    // Start is called before the first frame update
    void Start()
        {
        _startPosition = transform.localPosition;
        _startRot = transform.localEulerAngles;
        FreqMultiplier = 1;
        AmplitudeModifier = 1;
        }

    // Update is called once per frame
    void Update()
        {
        float offsetFramecount = Time.time + _offset;
        Vector3 posTarget = new Vector3(_amplitude.x * Mathf.Sin(offsetFramecount * _frequency.x * FreqMultiplier), _amplitude.y * Mathf.Sin(offsetFramecount * _frequency.y * FreqMultiplier), _amplitude.z * Mathf.Sin(offsetFramecount * _frequency.z * FreqMultiplier)) * AmplitudeModifier;
        Vector3 rotTarget = new Vector3(_rotAmplitude.x * Mathf.Sin(offsetFramecount * _rotFrequency.x * FreqMultiplier), _rotAmplitude.y * Mathf.Sin(offsetFramecount * _rotFrequency.y * FreqMultiplier), _rotAmplitude.z * Mathf.Sin(offsetFramecount * _rotFrequency.z * FreqMultiplier)) * AmplitudeModifier;

        _posMov = Vector3.Lerp(_posMov, posTarget, _lerp * Time.deltaTime);
        _rotMov = Vector3.Lerp(_rotMov, rotTarget, _lerp * Time.deltaTime);

        transform.localPosition = _startPosition + _posMov;
        transform.localEulerAngles = _startRot + _rotMov;
        }
    }
