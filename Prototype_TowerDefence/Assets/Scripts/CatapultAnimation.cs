﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatapultAnimation : MonoBehaviour
{
    [SerializeField] private BuildingBehaviour _catapultBB;
     // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Animator>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_catapultBB.TowerState != TowerState.Constructing)
        {
            Debug.Log("check");
            {
                this.GetComponent<Animator>().enabled = true;
                this.enabled = false;
            }
        }
    }
}
