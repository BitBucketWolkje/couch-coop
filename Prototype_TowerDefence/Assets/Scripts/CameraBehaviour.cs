﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraBehaviour : MonoBehaviour
{
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _smoothDamp = .5f, _maxZoom = 10, _minZoom = 50;

    private Camera _cam;

    public static readonly List<Transform> _targets = new List<Transform>();

    Vector3 _velocity = Vector3.zero;

    void Start()
    {
        _cam = GetComponent<Camera>();
    }

    void Update()
    {
        GetTargets();
    }

    private void LateUpdate()
    {
        if (_targets.Count == 0) return;
        Move();
        Zoom();
        _targets.Clear();
    }

    private void Zoom()
    {
        float newZoom = Mathf.Lerp(_maxZoom, _minZoom, GetGreatestDistance() / 50f);
        _cam.fieldOfView = Mathf.Lerp(_cam.fieldOfView, newZoom, Time.deltaTime);
    }

    private float GetGreatestDistance()
    {
        Bounds bounds = GetBounds();
        return bounds.size.x + bounds.size.z;
    }

    private void Move()
    {
        Vector3 centerPoint = GetCenterTargets();
        Vector3 newPosition = centerPoint + _offset;
        transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref _velocity, _smoothDamp);
    }

    private Vector3 GetCenterTargets()
    {
        if (_targets.Count == 1) return _targets[0].position;
        Bounds bounds = GetBounds();
        return bounds.center;
    }

    private Bounds GetBounds()
    {
        var bounds = new Bounds(_targets[0].position, Vector3.zero);
        for (int i = 0; i < _targets.Count; i++)
        {
            bounds.Encapsulate(_targets[i].position);
        }

        return bounds;
    }

    private void GetTargets()
    {
        foreach (GameObject gameObject in GameObject.FindGameObjectsWithTag("Player"))
        {
            _targets.Add(gameObject.transform);
        }

        //for (int i = 1; i <= 3; i++)
        //{
        //    if (GameObject.FindGameObjectWithTag("Player" + i) != null)
        //        _targets.Add(GameObject.FindGameObjectWithTag("Player" + i).transform);
        //}
    }
}