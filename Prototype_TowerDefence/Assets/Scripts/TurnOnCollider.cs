﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class TurnOnCollider : MonoBehaviour
{
    [SerializeField] private BoxCollider _triggerBox;
    [SerializeField] private BoxCollider _fallBox;
    [SerializeField] private GameObject _particleSystem;

    private Transform _startPos;
    private Transform _endPos;
    private PathFindingBehaviour _pathFindingBehaviour;

    private void Start()
    {
        _pathFindingBehaviour = GameObject.FindGameObjectWithTag("PFM").GetComponent<PathFindingBehaviour>();
        _startPos = GameObject.FindGameObjectWithTag("WaveSpawner").transform;
        _endPos = GameObject.FindGameObjectWithTag("Target").transform;
        this.GetComponent<Rigidbody>().velocity = Vector3.down * 20;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
           if (!this.CompareTag("Wall"))
           {
               BuildingBehaviour bb = this.GetComponentInChildren<BuildingBehaviour>();
               if (bb.TowerState != TowerState.Constructing)
               {
                   _triggerBox.isTrigger = false;
                   this.enabled = false;

               }
           }
           else
           {
            _triggerBox.isTrigger = false;
           }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Terrain"))
        {
            StartCoroutine(PlayParticles());
            this.GetComponent<Rigidbody>().isKinematic = true;
            _fallBox.enabled = false;
        }
        StartCoroutine(CheckPath());

    }

    private IEnumerator PlayParticles()
    {
        _particleSystem.SetActive(true);
        yield return  new WaitForSeconds(1f);
        _particleSystem.SetActive(false);
    }

    private IEnumerator CheckPath()
    {
        yield return new WaitForEndOfFrame();
        CheckIfStillPassable();

    }

    private void CheckIfStillPassable()
    {
        if (_pathFindingBehaviour.IsPathPosible(_startPos.position, _endPos.position))
        {
            return;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}


