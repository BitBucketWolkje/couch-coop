﻿using System;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class Motor
    {
        private CharacterController _characterController;
        private float _dragCoefficient = 0.4f;

        private Vector3 _velocity = Vector3.zero;

        private float _maxRunningSpeed = (30.0f * 1000) / (60 * 60); // [m/s], 30 km/h

        private float _resetVelocityTimer = 1.5f;
        
        private float _mass = 75; // [kg] 

        private float _acceleration = 5; // [m/s^2] 

        private Transform _absoluteForward;

        private GameObject _characterModel;

        private float _rotationSpeed = 1000f;


    public Motor(CharacterController characterController, Transform absoluteForward, GameObject characterModel)
        {
            _characterController = characterController;
            _absoluteForward = absoluteForward;
            _characterModel = characterModel;
        }
        public void Run(Vector3 movement)
        {
        ApplyXZFriction(_dragCoefficient);
        ResetVelocity(movement);
        ApplyGravity();
        ApplyGround();
        ApplyMovement(movement);
        LimitMaximumSpeed();
        RotateCharacter();
        _characterController.Move(_velocity * Time.deltaTime);
        }

        

    private void LimitMaximumSpeed()
    {
        Vector3 yVelocity = Vector3.Scale(_velocity, new Vector3(0, 1, 0)); Vector3 xzVelocity = Vector3.Scale(_velocity, new Vector3(1, 0, 1));

        Vector3 clampedXzVelocity = Vector3.ClampMagnitude(xzVelocity, _maxRunningSpeed);

        _velocity = yVelocity + clampedXzVelocity;
    }

    //used to prevent the velocity values from going berserk after movement
    private void ResetVelocity(Vector3 movement)
    {
        if (movement.magnitude == 0)
        {
            _resetVelocityTimer -= Time.fixedDeltaTime;
        }
        else
        {
            _resetVelocityTimer = 1.5f;
        }

        if (_resetVelocityTimer <= 0)
        {
            _velocity = new Vector3(0, _velocity.y, 0);
        }
    }

    private void ApplyXZFriction(float surfaceFrictionCoefficient)
    {
        float preservedYVelocity = _velocity.y;

        // Only apply friction when the character movement controllers are untouched
        _velocity += GetFrictionForceVector(surfaceFrictionCoefficient) * Time.fixedDeltaTime;
        _velocity.y = preservedYVelocity;
    }

    private Vector3 GetFrictionForceVector(float surfaceFrictionCoefficient)
    {
        float frictionCoefficient = surfaceFrictionCoefficient;

        // Calculate kinetic friction force		https://www.softschools.com/formulas/physics/kinetic_friction_formula/92/
        Vector3 gravitationForce = Physics.gravity * _mass; // Fg = m * g (gravitationforce = mass * gravity) [in Newton]
        Vector3 normalForce = -gravitationForce; // η = - Fg (normalforce = - gravitationforce) [in Newton]
        Vector3 kineticFrictionForce = normalForce * frictionCoefficient; // Fk = μk mg (kinetic friction force = coefficient of kinetic friction * normal force) [in Newton]

        // Calculate (negative) acceleration due to friction force
        float frictionForceMagnitude = kineticFrictionForce.magnitude; // Transform vector to float
        float frictionForceAcceleration = frictionForceMagnitude / _mass; // a = f / m (acceleration = force / mass) [in m/s²]

        // Apply the friction force to the inverse direction of the current movement
        Vector3 frictionForceDirection = -_velocity;
        Vector3 frictionForce = frictionForceDirection * frictionForceAcceleration;

        return frictionForce;
    }

    private void ApplyMovement(Vector3 movement)
    {
        if (_characterController.isGrounded)
        {
            Vector3 xzAbsoluteForward = Vector3.Scale(_absoluteForward.forward, new Vector3(1, 0, 1));
            Quaternion forwardRotation = Quaternion.LookRotation(xzAbsoluteForward);
            Vector3 relativeMovement = forwardRotation * movement;
            _velocity += relativeMovement * _acceleration * Time.deltaTime;
        }
    }

    private void ApplyGround()
    {
        if (_characterController.isGrounded)
        {
            _velocity -= Vector3.Project(_velocity, Physics.gravity.normalized);
        }
    }

    private void ApplyGravity()
    {
        _velocity += Physics.gravity * Time.deltaTime;
    }
    // Rotates the character so it faces the direction it's moving
    private void RotateCharacter()
    {
        Vector3 XZVelocity = new Vector3(_velocity.x, 0, _velocity.z);

        if (XZVelocity.magnitude > 0.2f) // Prevent y rotation from resetting to 0 when there's no input
        {
            Quaternion currentRotation = _characterModel.transform.rotation;

            Quaternion lookRotation = Quaternion.LookRotation(_velocity);
            lookRotation = Quaternion.Euler(0f, lookRotation.eulerAngles.y, 0f);

            _characterModel.transform.rotation = Quaternion.Lerp(currentRotation, lookRotation, Time.deltaTime * _rotationSpeed);
        }
    }
}

