﻿using System;
using System.Collections;
using UnityEngine;

public struct Ingredient
{
    public GameObject Wood;
    public GameObject Stone;
}

[RequireComponent(typeof(CharacterController))]
public class CharacterControllerBehaviour : MonoBehaviour
{
    private BuildingBehaviour _building;

    [SerializeField]
    private WorkshopBehaviour _workshop;

    [SerializeField]
    private GameObject _characterModel;

    private Vector3 _movement = Vector3.zero;

    private float _speed = 2f;

    public PlayerSettings playerSettings;

    [SerializeField]
    private GameObject _pickUpObjectSlot;

    [SerializeField]
    public int JoystickNumber { get; set; }

    //############################REFACTORING##################################

    private Ingredient _ingredient;
    public StartUp StartUp;
    public GameObject PickedUpGameObject;

    private Motor _motor;

    private bool _interact;

    private float _radius = 4;
    public void Activate(int joystickNumber)
    {
        this.enabled = true;
        JoystickNumber = joystickNumber;
    }

    void Start()
    {
        _motor = new Motor(this.GetComponent<CharacterController>(), this.transform, _characterModel);
        _workshop = StartUp.Workshop;

        _ingredient.Stone = StartUp.Stone;
        _ingredient.Wood = StartUp.Wood;

        _speed = playerSettings.Speed;
    }

    void Update()
    {
        //Debug.Log(_cameraBehaviour.Level);
        Vector3 cameraRight = Camera.main.transform.right;
        cameraRight.y = 0;
        cameraRight = cameraRight.normalized;

        Vector3 cameraForward = Camera.main.transform.forward;
        cameraForward.y = 0;
        cameraForward = cameraForward.normalized;

        if (Input.GetButtonDown("Button_A" + JoystickNumber))
        {
            _interact = true;
        }
        if (Input.GetButtonUp("Button_A" + JoystickNumber))
        {
            _interact = false;
        }


        Vector3 movementX = cameraRight * Input.GetAxis("LStickHorizontal" + JoystickNumber);
        Vector3 movementZ = cameraForward * Input.GetAxis("LStickVertical" + JoystickNumber);

     
        _movement = (movementX + movementZ) * _speed;
    }


    private void FixedUpdate()
    {
        _motor.Run(_movement);
    }

    public void PickUpObject(GameObject ingredient)
    {
        PickedUpGameObject = Instantiate(ingredient, _pickUpObjectSlot.transform);
    }

    private void StoreObject()
    {
        Destroy(PickedUpGameObject);
    }

    public void AddToTower(BuildingBehaviour building, GameObject buildPiece)
    {
        _building = building;
        if (buildPiece != null && building != null)
        {
            if (buildPiece.tag == _ingredient.Wood.tag)
            {
                if (_building.StoredWood >= _building.RequiredWood)
                {
                    return;
                }
                _building.ConstructTurret(1, 0);       //not checking if building is already built
            }
            else if (buildPiece.tag == _ingredient.Stone.tag)
            {
                if (_building.StoredStone >= _building.RequiredStone)
                {
                    return;
                }
                _building.ConstructTurret(0, 1);
            }
        }
        Destroy(PickedUpGameObject);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("NormalTower") && Vector3.Distance(other.transform.position, transform.position) < _radius)
        {
            _building = other.GetComponent<BuildingBehaviour>();
            if (_building != null)
            {
                if (_building.TowerState != TowerState.Constructing)
                {
                    //return;
                }
                else
                {
                    if (_interact)
                    {
                        AddToTower(_building, PickedUpGameObject);
                        //return;
                    }
                }
            }
        }
        else if (other.CompareTag("FreezeTower") && Vector3.Distance(other.transform.position, transform.position) < _radius)
        {
            _building = other.GetComponent<BuildingBehaviour>();
            if (_building != null)
            {
                if (_building.TowerState != TowerState.Constructing)
                {
                    //return;
                }
                else
                {
                    if (_interact)
                    {
                        AddToTower(_building, PickedUpGameObject);
                        //return;
                    }
                }
            }
        }
        else if (other.CompareTag("FireTower") && Vector3.Distance(other.transform.position, transform.position) < _radius)
        {
            _building = other.GetComponent<BuildingBehaviour>();
            if (_building != null)
            {
                if (_building.TowerState != TowerState.Constructing)
                {
                    //return;
                }
                else
                {
                    if (_interact)
                    {
                        AddToTower(_building, PickedUpGameObject);
                        //return;
                    }
                }
            }
        }
        else if (other.CompareTag("BigCannon") && Vector3.Distance(other.transform.position, transform.position) < _radius)
        {
            _building = other.GetComponent<BuildingBehaviour>();
            if (_building != null)
            {
                if (_building.TowerState != TowerState.Constructing)
                {
                    //return;
                }
                else
                {
                    if (_interact)
                    {
                        AddToTower(_building, PickedUpGameObject);
                        //return;
                    }
                }
            }
        }
        else if (other.CompareTag("Catapult") && Vector3.Distance(other.transform.position, transform.position) < _radius)
        {
            _building = other.GetComponent<BuildingBehaviour>();
            if (_building != null)
            {
                if (_building.TowerState != TowerState.Constructing)
                {
                    //return;
                }
                else
                {
                    if (_interact)
                    {
                        AddToTower(_building, PickedUpGameObject);
                        //return;
                    }
                }
            }
        }
        else if (other.CompareTag("Wall") && Vector3.Distance(other.transform.position, transform.position) < _radius)
        {
            _building = other.GetComponent<BuildingBehaviour>();
            if (_building != null)
            {
                if (_interact)
                    {
                        AddToTower(_building, PickedUpGameObject);
                        //return;
                    }
            }
        }

        else
        {
            _building = null;
        }

        if (other.CompareTag("WoodStorage"))
        {
            WoodStorageBehaviour woodStorage = other.GetComponent<WoodStorageBehaviour>();

            if (_interact)
            {
                if (PickedUpGameObject != null && PickedUpGameObject.tag == _ingredient.Wood.tag)
                {
                    StoreObject();
                    WoodStorageBehaviour.WoodCount++;
                    _interact = false;
                    return;
                }
                else if (PickedUpGameObject == null && WoodStorageBehaviour.WoodCount > 0)
                {
                    PickUpObject(StartUp.Wood);
                    WoodStorageBehaviour.WoodCount--;
                    _interact = false;
                    return;
                }
            }
        }

        if (other.CompareTag("StoneStorage"))
        {
            StoneStorageBehaviour stoneStorage = other.GetComponent<StoneStorageBehaviour>();

            if (_interact)
            {
                if (PickedUpGameObject == null && StoneStorageBehaviour.StoneCount > 0)
                {
                    PickUpObject(StartUp.Stone);
                    StoneStorageBehaviour.StoneCount--;
                    _interact = false;
                    return;
                }
                else if (PickedUpGameObject != null && PickedUpGameObject.tag == _ingredient.Stone.tag)
                {
                    StoreObject();
                    StoneStorageBehaviour.StoneCount++;
                    _interact = false;
                    return;
                }
            }
        }

        if (other.CompareTag("Workshop"))
        {
            //_workshop = other.GetComponent<WorkshopBehaviour>();
            if (JoystickNumber != 0)
            {
                if (Input.GetButtonDown("Button_LB" + JoystickNumber))
             {
                 if (_workshop.BlueprintIndex > 0)
                 {
                     _workshop.BlueprintIndex--;
                     _workshop.ShouldUpdateBlueprint = true;
                 }
                 else
                 {
                     _workshop.BlueprintIndex = _workshop.BlueprintLength - 1;
                     _workshop.ShouldUpdateBlueprint = true;
                 }
             }
             else if (Input.GetButtonDown("Button_RB" + JoystickNumber))
             {
                 _workshop.BlueprintIndex++;
                 _workshop.BlueprintIndex %= _workshop.BlueprintLength;
                 _workshop.ShouldUpdateBlueprint = true;
             }
            }
        }
    }
}