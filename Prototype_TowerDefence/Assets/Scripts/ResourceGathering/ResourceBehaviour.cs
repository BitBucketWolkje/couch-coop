﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ResourceBehaviour : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _particle;

    [SerializeField]
    private Canvas _canvas;

    [SerializeField]
    private Slider _healthBar;

    [SerializeField]
    private Slider _progressBar;

    [SerializeField]
    private int _maxHitpoints;

    [SerializeField]
    private float _miningTime;

    private CharacterControllerBehaviour _characterController;
    private StartUp _startUp;

    private bool _isPlayerNear;

    private string _tag;

    private float _timer;

    private int _hitpoints;

    private void Start()
    {
        _tag = gameObject.tag;
        _hitpoints = _maxHitpoints;
        _healthBar.maxValue = _maxHitpoints;
        _progressBar.maxValue = _miningTime;
    }

    private void Update()
    {
        CheckIfPlayerIsNearAndInteracting();
        UpdateHealthAndProgressBar();
        DestroyWhenFullyMined();
        RotateCanvasTowardsCamera();
    }

    private void CheckIfPlayerIsNearAndInteracting()
    {
        if (_isPlayerNear)
        {
            if (Input.GetButton("Button_A" + _characterController.JoystickNumber))
            {
                DisplayProgressBar();
                MineResource();
            }
            else
            {
                _timer = 0;
                _progressBar.gameObject.SetActive(false);
            }
        }
    }

    private void DisplayProgressBar()
    {
        if (_characterController.PickedUpGameObject == null)
            _progressBar.gameObject.SetActive(true);
        else
            _progressBar.gameObject.SetActive(false);
    }

    private void MineResource()
    {
        if (_timer < _miningTime)
            _timer += Time.deltaTime;

        if (_timer >= _miningTime)
        {
            if (_tag == "Tree" && _characterController.PickedUpGameObject == null)
            {
                _characterController.PickUpObject(_startUp.Wood);
                _particle.Play();
                _hitpoints--;
                if (FindObjectOfType<AudioManagerScript>() != null) FindObjectOfType<AudioManagerScript>().Play("Wood");

            }
            else if (_tag == "Rock" && _characterController.PickedUpGameObject == null)
            {
                _characterController.PickUpObject(_startUp.Stone);
                _particle.Play();
                _hitpoints--;
                if (FindObjectOfType<AudioManagerScript>() != null) FindObjectOfType<AudioManagerScript>().Play("Stone");

            }

            _timer = 0;
        }
    }

    private void UpdateHealthAndProgressBar()
    {
        if (_hitpoints < _maxHitpoints)
            _healthBar.gameObject.SetActive(true);

        if (_isPlayerNear)
            _healthBar.gameObject.SetActive(true);
        else if (!_isPlayerNear && _hitpoints == _maxHitpoints)
            _healthBar.gameObject.SetActive(false);

        _progressBar.value = _timer;
        _healthBar.value = _hitpoints;
    }

    private void DestroyWhenFullyMined()
    {
        if (_hitpoints == 0)
        {
            this.GetComponent<Rigidbody>().isKinematic = false;
            this.GetComponent<Rigidbody>().velocity = Vector3.up * 50;
        }
    }

    private void RotateCanvasTowardsCamera()
    {
        _canvas.transform.LookAt(Camera.main.transform);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _characterController = other.GetComponent<CharacterControllerBehaviour>();
            _startUp = _characterController.StartUp;
            _isPlayerNear = true;
        }

        if (other.gameObject.CompareTag("Tile"))
        {
            other.GetComponent<TileBehaviour>().TileState = TileState.Full;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        this.GetComponent<Rigidbody>().isKinematic = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _characterController = null;
            _startUp = null;
            _isPlayerNear = false;
        }
        if (other.gameObject.CompareTag("Tile"))
        {
            other.GetComponent<TileBehaviour>().TileState = TileState.Empty;
        }
    }

    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }
}
