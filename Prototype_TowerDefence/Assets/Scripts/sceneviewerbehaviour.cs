﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneviewerbehaviour : MonoBehaviour
{
    [SerializeField] Transform[] sceneLocations;
    private Vector3[] _positions;
    private int _currentSceneSelected = 1;
    [SerializeField ] private bool _hasBeenPushed;

    private void Start()
    {
        _positions = new Vector3[sceneLocations.Length];
        for (int i = 0; i < sceneLocations.Length; i++)
        {
            _positions[i] = sceneLocations[i].localPosition;
        }
        //   transform.position += new Vector3( sceneLocations[_currentSceneSelected].position.x,0,0);
    }

    
    private void Update()
    {
        if(Input.GetAxisRaw("Horizontal") > 0.5f && !_hasBeenPushed)
        {
            ChangeCurrentScene(-1);
            _hasBeenPushed = true;
        }
        else if (Input.GetAxisRaw("Horizontal") < -0.5f && !_hasBeenPushed)
        {
            ChangeCurrentScene(1);
            _hasBeenPushed = true;
        }
        else if (Input.GetAxisRaw("Horizontal") >-.5f && Input.GetAxisRaw("Horizontal") < .5f && _hasBeenPushed)
        {
            _hasBeenPushed = false;
        }

       if (Input.GetButtonDown("Button_A1"))
       {
           switch (_currentSceneSelected)
           
           {
                case 0:
                    SceneManager.LoadScene(2);
                    break;
                case 1:
                    SceneManager.LoadScene(1);
                    break;
                case 2:
                    SceneManager.LoadScene(3);
                    break;
            }
        }
    }

    private void ChangeCurrentScene(int direction)
    {
        _currentSceneSelected += direction;
        _currentSceneSelected %= sceneLocations.Length;
        if (_currentSceneSelected < 0) _currentSceneSelected = sceneLocations.Length - 1;


        LeanTween.moveLocalX(gameObject, _positions[_currentSceneSelected].x, 0.5f);
        
    }

}
