﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileState { Empty, Full }

public class TileBehaviour : MonoBehaviour
{
    private MeshRenderer _tileRenderer;

    private CharacterControllerBehaviour _player;

    private float _timer = 0f;
    private float _maxTimer = 1f;
    private bool _aHeld = false;

    [SerializeField] private GameObject _placeSpot;

    public TileState TileState = TileState.Empty;


    private StartUp _startUp;


    private WorkshopBehaviour _workshop;


    private void Start()
    {
        _startUp = GameObject.FindGameObjectWithTag("StartUp").GetComponent<StartUp>();
        _workshop = _startUp.Workshop;
        _tileRenderer = this.transform.GetComponentInChildren<MeshRenderer>();
    }

    private void Update()
    {
        if (_player != null)
        {
            if (Input.GetButton("Button_A" + _player.JoystickNumber))
            {
                _aHeld = true;
            }

            if (Input.GetButtonUp("Button_A" + _player.JoystickNumber))
            {
                _aHeld = false;
                _timer = 0f;
            }

            if (_aHeld)
            {
                _timer += Time.deltaTime;
            }
        }
      }


    private void OnTriggerEnter(Collider other)
    {
            if (other.gameObject.CompareTag("Player"))
            {
                _timer = 0f;
                _player = other.GetComponent<CharacterControllerBehaviour>();
            }
    }
    private void OnTriggerStay(Collider other)
    {
            if (TileState == TileState.Empty && _player != null)
            {
                if ((_player.PickedUpGameObject != null) && other.gameObject.CompareTag("Player"))
                {
                    _placeSpot.SetActive(true);

                    if (_timer >= _maxTimer)
                    {
                        _timer = 0f;



                        if (_player.PickedUpGameObject.CompareTag("Wood"))      //THIS CAN BE BETTER
                        {
                            if (_workshop.CurrentBuilding.CompareTag("Wall"))
                            {
                                return;
                            }
                             GameObject tower = Instantiate(_workshop.CurrentBuilding, this.transform.position + new Vector3(0, 20f, 0), this.transform.rotation);
                             TileState = TileState.Full;
                            _player.AddToTower(tower.GetComponentInChildren<BuildingBehaviour>(), _startUp.Wood);
                        }
                        else
                        {
                            GameObject tower = Instantiate(_workshop.CurrentBuilding, this.transform.position + new Vector3(0, 20f, 0), this.transform.rotation);
                            TileState = TileState.Full;
                        _player.AddToTower(tower.GetComponentInChildren<BuildingBehaviour>(), _startUp.Stone);
                        }

                    }
                }
            }
    }

    private void OnTriggerExit(Collider other)
    {
            if (other.gameObject.CompareTag("Player"))
            {
                _placeSpot.SetActive(false);
                _player = null;
            }
            else
            {
                TileState = TileState.Empty;
            }
    }
}
