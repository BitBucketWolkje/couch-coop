﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LivesChangedArgs : EventArgs
{

    public LivesChangedArgs(int Lives, CastleScript castle)
    {
        if (Lives <= 0)
        {
            castle.DestroyCastle();
        }
    }
}
public class CastleScript : MonoBehaviour
{
    private int _lives;
    public int StartingLives = 10;

    private event EventHandler<LivesChangedArgs> LivesChanged;

   [SerializeField] private healthBarScript healthBarScript;
    [SerializeField] private GameObject _gameOver;
    [SerializeField] private Transform _canvas;


    public int Lives
    {
        get
        { return _lives; }
        set
        {
            _lives = value;
            LivesChanged?.Invoke(this, new LivesChangedArgs(_lives, this));
            if (_lives == 0) Instantiate(_gameOver, _canvas);
            healthBarScript.UpdateBar((float)_lives / (float)StartingLives);
            if (FindObjectOfType<AudioManagerScript>() != null) FindObjectOfType<AudioManagerScript>().Play("CastleHit");
        }
    }
    private void Start()
    {
        _lives = StartingLives;
    }

    public void DestroyCastle()
    {
        Debug.Log("Game Over");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Lives = Lives - other.GetComponent<EnemyBehaviour>()._damage;
            Destroy(other.gameObject);


        }
    }

}
