﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WorkshopBehaviour : MonoBehaviour
{
    public GameObject CurrentBuilding;

    public int BlueprintIndex;
    public bool ShouldUpdateBlueprint;

    public int BlueprintLength { get => _buildingBlueprints.Length; }

    [SerializeField] private GameObject[] _buildingBlueprints;
    [SerializeField] private Mesh[] _blueprintMeshes;
    [SerializeField] private MeshFilter _meshFilter;


    private void Start()
    {
        _meshFilter.sharedMesh = _blueprintMeshes[BlueprintIndex];
    }

    void Update()
    {
        if (ShouldUpdateBlueprint)
        {
            CurrentBuilding = _buildingBlueprints[BlueprintIndex];
            _meshFilter.sharedMesh = _blueprintMeshes[BlueprintIndex];
            ShouldUpdateBlueprint = false;
        }

        if (CurrentBuilding.CompareTag("Wall"))
        {
          this.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = ":" + CurrentBuilding.transform.GetComponent<BuildingBehaviour>().RequiredStone.ToString();
          this.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = ":" + CurrentBuilding.transform.GetComponent<BuildingBehaviour>().RequiredWood.ToString();
        }
        else
        {
          this.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = ":" + CurrentBuilding.transform.GetChild(1).GetComponent<BuildingBehaviour>().RequiredStone.ToString();
          this.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = ":" + CurrentBuilding.transform.GetChild(1).GetComponent<BuildingBehaviour>().RequiredWood.ToString();
        }

        this.transform.GetChild(0).LookAt(Camera.main.transform);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Tile"))
        {
            other.GetComponent<TileBehaviour>().TileState = TileState.Full;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        this.GetComponent<Rigidbody>().isKinematic = true;
    }

}