﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WallBehaviour : MonoBehaviour
{
    private PathFindingBehaviour _pathFindingBehaviour;
    public int RequiredStone;
    public int StoredStone;
    private Transform _startPos;
    private Transform _endPos;

    public Action<Vector3[], bool> Callback;

    private IEnumerator Start()
    {
        _pathFindingBehaviour = GameObject.FindGameObjectWithTag("PFM").GetComponent<PathFindingBehaviour>();
        _startPos = GameObject.FindGameObjectWithTag("WaveSpawner").transform;
        _endPos = GameObject.FindGameObjectWithTag("Target").transform;

        yield return new WaitForEndOfFrame();
        CheckIfStillPassable();
    }

    private void CheckIfStillPassable()
    {
       if( _pathFindingBehaviour.IsPathPosible(_startPos.position, _endPos.position))
       {
           return;
       }
       else
       {
           Destroy(this.gameObject);
       }
    }

    private void OnTriggerExit(Collider other)
    {
        this.GetComponent<BoxCollider>().isTrigger = false;
    }
}
