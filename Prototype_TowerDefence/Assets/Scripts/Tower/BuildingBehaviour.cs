﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public enum TowerState
{
    Idle,
    Aiming,
    Shooting,
    Constructing,
}

public enum TowerType
{
    NormalTower,
    Wall,
    FreezeTower,
    FireTower, 
    BigCannon,
    Catapult
}

public class BuildingBehaviour : MonoBehaviour
{

    public int RequiredWood, RequiredStone;
    public int StoredWood, StoredStone;

    [SerializeField] private TextMeshProUGUI _woodStorageText;
    [SerializeField] private TextMeshProUGUI _stoneStorageText;

    public TowerState TowerState = TowerState.Constructing;
    public TowerType TowerType;

    private Vector3 _aimDirection;

    [SerializeField]
    private float _shootTimer = 0f;
    [SerializeField]
    private float _maxShootTimer = 1f;

    [SerializeField]
    private GameObject newBullet;

    [SerializeField]
    private Transform _towerBarrel;

    private Transform _startPos;
    private Transform _endPos;
    private PathFindingBehaviour _pathFindingBehaviour;

    public static List<Transform> EnemiesInRange = new List<Transform>();
    [SerializeField] private float gravity = 0f;




    private void Start()
    {
        _pathFindingBehaviour = GameObject.FindGameObjectWithTag("PFM").GetComponent<PathFindingBehaviour>();
        _startPos = GameObject.FindGameObjectWithTag("WaveSpawner").transform;
        _endPos = GameObject.FindGameObjectWithTag("Target").transform;
    }

    void Update()
    {
        Debug.Log(this.TowerState);
          if (this.TowerType != TowerType.Wall)
          {
          UpdateTowerUI();


          switch (TowerState)
          {
              case TowerState.Aiming:

                  Aim();
                  if (_shootTimer >= _maxShootTimer)
                  {
                      TowerState = TowerState.Shooting;
                  }
                    break;
              case TowerState.Shooting:
                  Shoot();
                  break;
              case TowerState.Constructing:
 

                    CheckWhenBuilt();

                  break;
           
          }

          if (this.TowerType == TowerType.NormalTower || this.TowerType == TowerType.BigCannon)
          {
              this.GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0,Mathf.Min((_shootTimer/_maxShootTimer)*100,100));
          }

          

          this.transform.forward = Vector3.Lerp(this.transform.forward, _aimDirection, 0.99f);
        }
    }

    private void CheckWhenBuilt()
    {
        if (RequiredWood <= StoredWood && RequiredStone <= StoredStone)
        {
            this.transform.parent.GetChild(0).gameObject.SetActive(false);          // UI afzetten als toren klaar is 😍😍
            TowerState = TowerState.Idle;
            if (this.CompareTag("FireTower") || this.CompareTag("FreezeTower"))
            {

                this.transform.GetChild(0).gameObject.SetActive(true);
                this.transform.GetChild(1).gameObject.SetActive(true);
                this.transform.GetChild(2).gameObject.SetActive(true);
                this.transform.GetChild(3).gameObject.SetActive(true);
            }

            foreach (var Enemy in WaveManager.ENEMYLIST)
            {
                Enemy.ShouldRequestPath = true;
            }
        }
    }



    public void ConstructTurret(int wood, int stone)
    {
        if (TowerState == TowerState.Constructing)
        {
            StoredWood += wood;
            StoredStone += stone;      //kan veel properder geschreven worden
        }
    }

    private void Shoot()
    {
        if (this.TowerType == TowerType.BigCannon || this.TowerType == TowerType.NormalTower)
        {
            Instantiate(newBullet, _towerBarrel.position, this.transform.rotation);
        }

        if (this.TowerType == TowerType.Catapult)
        {
           GameObject go =  Instantiate(newBullet, _towerBarrel.position, this.transform.rotation);
          Rigidbody rb =  go.GetComponent<Rigidbody>();
          rb.velocity = CalculatePath(rb, EnemiesInRange[0]);
        }

        if (this.TowerType == TowerType.FireTower)
        {
            foreach (var Enemy in EnemiesInRange)
            {
                Enemy.GetComponent<EnemyBehaviour>().OnFire = true;
            }
        }

        if (this.TowerType == TowerType.FreezeTower)
        {
            foreach (var Enemy in EnemiesInRange)
            {
                Enemy.GetComponent<PathingBehaviour>().Frozen = true;
            }
        }

            _shootTimer = 0f;

            TowerState = TowerState.Aiming;
    }

    private void OnTriggerStay(Collider other)
    {
        if (TowerState == TowerState.Constructing)
        {
            return;
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            if (!EnemiesInRange.Contains(other.transform))
            {
            TowerState = TowerState.Aiming;
                EnemiesInRange.Add(other.transform);
            }
        }

    }


    private void OnTriggerExit(Collider other)
    {
        if (TowerState != TowerState.Constructing)
        {
            if (other.gameObject.CompareTag("Enemy"))
            {
                TowerState = TowerState.Idle;
                EnemiesInRange.Remove(other.transform);
                StartCoroutine(other.GetComponent<EnemyBehaviour>().ExtinguishFire());
                StartCoroutine(other.GetComponent<PathingBehaviour>().Defrost());
            }
        }
    }

    private void Aim()
    {
        if (this.TowerType == TowerType.BigCannon || this.TowerType == TowerType.NormalTower ||
            this.TowerType == TowerType.Catapult)
        {
            if (EnemiesInRange.Count > 0)
             {
                 if (EnemiesInRange[0] == null)
                 {
                     EnemiesInRange.Clear();
                     return;
                 }
                 Vector3 aimDirection = EnemiesInRange[0].position - this.transform.position;
                 aimDirection = Vector3.ProjectOnPlane(aimDirection, Vector3.up);
                 _aimDirection = aimDirection;
                 this.transform.rotation = Quaternion.LookRotation(_aimDirection);
             }
             else
             {
                 TowerState = TowerState.Idle;
             }
        }
        _shootTimer += Time.deltaTime;
    }

    private void UpdateTowerUI()
    {
        _woodStorageText.text = StoredWood.ToString() + "/" + RequiredWood.ToString();
        _stoneStorageText.text =StoredStone.ToString() + "/" + RequiredStone.ToString();

        _woodStorageText.transform.LookAt(Camera.main.transform);
        _stoneStorageText.transform.LookAt(Camera.main.transform);
    }
    
    public Vector3 CalculatePath(Rigidbody ball, Transform target)
    {

        float displacementY = target.position.y - ball.position.y;
        Vector3 displacementXZ = new Vector3(target.position.x - ball.position.x, 0, target.position.z - ball.position.z);
        float time = Mathf.Sqrt(-2 * 10 / -gravity) + Mathf.Sqrt(2 * (displacementY - 10) / -gravity);
        Vector3 velocityY = Vector3.up * Mathf.Sqrt(-2 * -gravity * 10);
        Vector3 velocityXZ = displacementXZ / (Mathf.Sqrt(-2 * 10 / -gravity) + Mathf.Sqrt(2 * (displacementY - 10) / -gravity));

        return velocityXZ + velocityY;
    }


}