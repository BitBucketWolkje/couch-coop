﻿using UnityEngine;

public class MaterialSwitchingBehaviour : MonoBehaviour
{
    [SerializeField] private BuildingBehaviour _buildingBehaviour;

    [SerializeField] private Renderer _baseRenderer;
    [SerializeField] private Renderer _turntableRenderer;
    [SerializeField] private Renderer _armRenderer;
    
    [SerializeField] private Material _blueprintMaterial;

    private Material[] _baseMaterials;
    private Material[] _turntableMaterials;
    private Material[] _armMaterials;
    private Material[] _storedBaseMaterials;
    private Material[] _storedTurntableMaterials;
    private Material[] _storedArmMaterials;

    private bool _canSwitchMaterial = true;

    private void Start()
    {
        CollectCurrentMaterials();
        AssignBlueprintMaterial();
    }

    private void CollectCurrentMaterials()
    {
        _baseMaterials = _baseRenderer.sharedMaterials;
        _storedBaseMaterials = _baseRenderer.sharedMaterials;

        if (_turntableRenderer != null)
        {
            _turntableMaterials = _turntableRenderer.sharedMaterials;
            _storedTurntableMaterials = _turntableRenderer.sharedMaterials;
        }

        if (_armRenderer != null)
        {
            _armMaterials = _armRenderer.sharedMaterials;
            _storedArmMaterials = _armRenderer.sharedMaterials;
        }
    }

    private void AssignBlueprintMaterial()
    {
        if (_buildingBehaviour.TowerState == TowerState.Constructing)
        {
            for (int i = 0; i < _baseMaterials.Length; i++)
                _baseMaterials[i] = _blueprintMaterial;
           
            if (_turntableRenderer != null)
            {
                for (int i = 0; i < _turntableMaterials.Length; i++)
                    _turntableMaterials[i] = _blueprintMaterial;
            }

            if (_armRenderer != null)
            {
                for (int i = 0; i < _storedArmMaterials.Length; i++)
                    _armMaterials[i] = _blueprintMaterial;
            }
        }

        _baseRenderer.sharedMaterials = _baseMaterials;

        if (_turntableRenderer != null)
            _turntableRenderer.sharedMaterials = _turntableMaterials;

        if (_armRenderer != null)
            _armRenderer.sharedMaterials = _armMaterials;
    }

    private void Update()
    {
        SwitchToNormalMaterials();
    }

    private void SwitchToNormalMaterials()
    {
        if (_buildingBehaviour.TowerState != TowerState.Constructing && _canSwitchMaterial)
        {
            if (FindObjectOfType<AudioManagerScript>() != null) FindObjectOfType<AudioManagerScript>().Play("Building");


            for (int i = 0; i < _baseMaterials.Length; i++)
                _baseMaterials[i] = _storedBaseMaterials[i];
            if (_turntableRenderer != null)
            {
                for (int i = 0; i < _turntableMaterials.Length; i++)
                    _turntableMaterials[i] = _storedTurntableMaterials[i];
            }

            if (_armRenderer != null)
            {
                for (int i = 0; i < _armMaterials.Length; i++)
                    _armMaterials[i] = _storedArmMaterials[i];
            }

            _baseRenderer.sharedMaterials = _baseMaterials;

            if (_turntableRenderer != null)
                _turntableRenderer.sharedMaterials = _turntableMaterials;

            if (_armRenderer != null)
                _armRenderer.sharedMaterials = _armMaterials;

            _canSwitchMaterial = false;
        }
    }
}