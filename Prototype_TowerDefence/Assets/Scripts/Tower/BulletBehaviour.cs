﻿using System;
using System.Collections;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    public event EventHandler HitEnemy;
    [SerializeField] private float _despawnTime;

    [SerializeField] private float _radius;
    [SerializeField] private GameObject _cannonExplosion;
    [SerializeField] private GameObject _catapultExplosion;

    public void Start()
    {
        StartCoroutine(DespawnTimer());
    }

    void Update()
    {
        if (this.CompareTag("Catapult"))
        {
            return;
        }
        this.transform.Translate(Vector3.forward * Time.deltaTime * 40f);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (this.CompareTag("ExplosiveBullet"))
        {
            StartCoroutine(Explosion(_cannonExplosion));
            Collider[] objects = Physics.OverlapSphere(this.transform.position, _radius);
           
            foreach (Collider C in objects)
            {
                   if (C.CompareTag("Enemy"))
                   {
                       C.GetComponent<EnemyBehaviour>().Health -= 0.5f;
                   }
            }
        }

        if (this.CompareTag("Catapult"))
        {
            StartCoroutine(Explosion(_catapultExplosion));
            Collider[] objects = Physics.OverlapSphere(this.transform.position, _radius);

            foreach (Collider C in objects)
            {
                if (C.CompareTag("Enemy"))
                {
                    C.GetComponent<EnemyBehaviour>().Health -= 0.25f;
                }
            }
        }
            if (other.gameObject.CompareTag("Enemy"))
            {
                other.gameObject.GetComponent<EnemyBehaviour>().Health -= 1;
            }

        Destroy(this.gameObject);
    }
        
        

    

    public IEnumerator DespawnTimer()
    {
        yield return new WaitForSeconds(_despawnTime);
        Destroy(this.gameObject);
    }

    public IEnumerator Explosion(GameObject explosion)
    {
        GameObject newExplosion = Instantiate(explosion, this.transform.position, this.transform.rotation);
        yield return new WaitForSeconds(1f);
        Destroy(newExplosion);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        Gizmos.DrawWireSphere(this.transform.position, _radius);
    }

   
}
