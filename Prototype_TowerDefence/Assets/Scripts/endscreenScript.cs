﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class endscreenScript : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreText;
    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = "Score: " + CoinUI.COINSCOLLECTED;
        LeanTween.scale(gameObject, new Vector3(1, 1, 1), 0.5f).setOnComplete(onComplete);
    }

    public void onComplete()
    {
        Time.timeScale = 0;
       if(FindObjectOfType<AudioManagerScript>()!= null) FindObjectOfType<AudioManagerScript>().Play("GameOver");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Button_A1"))
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }
    }
}
