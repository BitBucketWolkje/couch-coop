﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaitLobby : MonoBehaviour
{
    private Vector3 _movement = Vector3.zero;
    private Motor _motor;
    [SerializeField]
    private GameObject _characterModel;
    private float _speed = 12f;
    public int JoystickNumber { get; set; }


    // Start is called before the first frame update
    private void Start()
    {
        _motor = new Motor(this.GetComponent<CharacterController>(), this.transform, _characterModel);
    }
    void Update()
    {

        //Debug.Log(_cameraBehaviour.Level);
        Vector3 cameraRight = Camera.main.transform.right;
        cameraRight.y = 0;
        cameraRight = cameraRight.normalized;

        Vector3 cameraForward = Camera.main.transform.forward;
        cameraForward.y = 0;
        cameraForward = cameraForward.normalized;

        Vector3 movementX = cameraRight * Input.GetAxis("LStickHorizontal" + JoystickNumber);
        Vector3 movementZ = cameraForward * Input.GetAxis("LStickVertical" + JoystickNumber);


        _movement = (movementX + movementZ) * _speed;
    }

    public void Activate(int joystickNumber)
    {
        this.enabled = true;
        JoystickNumber = joystickNumber;
    }


    private void FixedUpdate()
    {
        _motor.Run(_movement);
    }

    public void StartGame()
    {
        this.GetComponent<CharacterControllerBehaviour>().enabled = true;
        this.GetComponent<CharacterControllerBehaviour>().Activate(JoystickNumber);
        this.enabled = false;
    }
}
