﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healthBarScript : MonoBehaviour
{
    public Transform positionToFollow;
    public Vector3 positionToFollowLastFrame;

    [SerializeField] bool isTurnedOn = false;

    [SerializeField] Slider bar;


    private bool firstTimeActivated = false;

    private void Start()
    {
        if (isTurnedOn) enable();
    }
    private void Update()
    {
        if(!isTurnedOn)
        transform.LookAt(Camera.main.transform.position);

        if (positionToFollow == null) Destroy(gameObject);

    }

    public void UpdateBar(float percent)
    {
        if (!firstTimeActivated && percent < 1)
        {
            enable();
        }

        bar.value = 3*percent;
    }

    private void enable()
    {
        bar.enabled = true;
        bar.gameObject.SetActive(true);
        firstTimeActivated = true;
    }
}
