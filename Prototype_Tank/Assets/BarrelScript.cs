﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelScript : MonoBehaviour
{
    [SerializeField]
    private Transform _barrelTranform;
    [SerializeField]
    private GameObject _bullet;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Rotation();

        Gun();

    }

    private void Gun()
    {
        if (UnityEngine.Input.GetButtonDown("B"))
            {
            Instantiate(_bullet, transform.position + new Vector3 ( 0,0.35f,1.285f), transform.rotation);
                }
    }

    private void Rotation()
    {

        //get left stick input
        float horizontalLeft = UnityEngine.Input.GetAxis("RightStick_Horizontal");
        float verticalLeft = UnityEngine.Input.GetAxis("RightStick_Vertical");

        // calculate the player movement direction
        Vector2 directionLeft = new Vector2(horizontalLeft, verticalLeft);


        Vector3 cameraForward = Vector3.Scale(Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 cameraRight = Vector3.Scale(Camera.main.transform.right, new Vector3(1, 0, 1)).normalized;

        Vector3 worldDirectionRight = cameraRight * directionLeft.x + cameraForward * directionLeft.y;

        ApplyRotation(worldDirectionRight);
    }

    public void ApplyRotation(Vector3 worldDirection)
    {
        Quaternion worldOrientation = Quaternion.LookRotation(worldDirection, Vector3.up);

        transform.rotation = worldOrientation;
    }
    private void LateUpdate()
    {
        transform.position = _barrelTranform.position;
    }
}
