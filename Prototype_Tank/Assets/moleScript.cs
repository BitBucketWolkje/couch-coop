﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moleScript : MonoBehaviour
{
    private bool _isDown = true;

    [SerializeField]
    private Vector3 _upLocation;
    private Vector3 _downLocation;
    [SerializeField]
    private float _timerStart = 0;
    private float _timer = 7;
    // Start is called before the first frame update
    void Start()
    {
        _downLocation = new Vector3(0,-10,0);
    }

    // Update is called once per frame
    void Update()
    {
        _timerStart += Time.deltaTime;
        if(_timerStart > _timer)
        {
            if (!_isDown)
            {
                Debug.Log("you Lost");
            }
            else if (_isDown)
            {
                transform.position = _upLocation;
                _isDown = false;
                _timerStart = 0;
                _timer = (10);
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            transform.position = _downLocation;
            _isDown = true;
            _timerStart = 0;
            _timer = Random.Range(5, 8);
        }
    }
}
