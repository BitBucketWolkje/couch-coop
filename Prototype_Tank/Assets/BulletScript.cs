﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    private float _timer = 0;
    [SerializeField]
    private Vector3 _velocity;
    // Update is called once per frame
    void Update()
    {
        transform.Translate(_velocity);
        _timer += Time.deltaTime;

        if(_timer > 5)
        {
            Destroy(gameObject);
        }
    }
}
