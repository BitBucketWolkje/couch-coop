﻿using System;
using TMPro;
using UnityEngine;


namespace Game
{
    [Serializable]
    public class MotorConfiguration
    {
        public float Speed;
        public float JumpHeight;
    }

    public class Motor
    {
        private readonly CharacterController _controller;

        private Vector3 _velocity;
        private MotorConfiguration _configuration;

        public Motor(CharacterController controller,  MotorConfiguration configuration)
        {
            _controller = controller;
            _configuration = configuration;
        }

        public void Begin()
        {
            _velocity = _controller.velocity;

            ApplyGravity();
        }

        private void ApplyGravity()
        {
            _velocity += Physics.gravity * Time.deltaTime;
        }

        public void Applyground(Vector3 groundNormal)
        {
            //only keep vertical velocity
            _velocity = Vector3.Scale(_velocity, new Vector3(0, 1, 0));

            //transform velocity into pure velocity along plane.
            _velocity = Vector3.ProjectOnPlane(_velocity, groundNormal);
        }

        public void ApplyMovement(Vector3 groundNormal, Vector3 worldDirection)
        {
            //calculate ground orientation
            Vector3 groundTangent = worldDirection;
            Vector3.OrthoNormalize(ref groundNormal, ref groundTangent);

            //add movement to velocity
            _velocity += groundTangent * worldDirection.magnitude * _configuration.Speed;


            Quaternion worldOrientation = Quaternion.LookRotation(worldDirection, Vector3.up);

            _controller.transform.rotation = worldOrientation;
        }

        public void ApplyJump()
        {
            _velocity = new Vector3(
                _velocity.x,
                Mathf.Sqrt(2 * Physics.gravity.magnitude * _configuration.JumpHeight),
                _velocity.z);
        }

        public void ApplyRotation(Vector3 worldDirection)
        {
            Quaternion worldOrientation = Quaternion.LookRotation(worldDirection, Vector3.up);

            _controller.transform.rotation = worldOrientation;
        }

        public void Commit()
        {
            _controller.Move(_velocity * Time.deltaTime);
        }


    }
}
